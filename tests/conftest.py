import pytest

from tddplatformpy import tddplatformpy


@pytest.fixture(scope='function', autouse=True)
def fb():
    '''3つ以上の重複が出てきたら、外出しを検討する。'''
    yield tddplatformpy.FizzBuzz()
