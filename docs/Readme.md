# FizzBuzz

At the beginning, tdd-platform-py contains fizzbuzz script and its tests and documents.

I made them when I was studying TDD from a youtube video, which was really good for me.

Link to the video is written here: 
[20210313_TDDBC2020.md](./20210313_TDDBC2020.md) .

Hope someone will be interested :) (Video is in Japanese.)
