# tdd-platform-py

Provides a directory structure to quickly start building a python project. No features by itself.

## Usage

1. Download this project.
2. If using Visual Studio Code with Dev Containers extension, open the project inside devcontainer.  
    If not, make sure python and packages in ./requirements.txt and ./requirements-dev.txt are installed.
3. Check that the sample test runs. Submit following commands at the project root directory.
    - `flake8 -v --ignore E402` (Should finish with 'reported 0'.)
    - `pytest -v` (All tests should pass.)
4. Change it to your own project.   
    - Change root directory name to your project's name.
    - Change ./Readme.md.
    - Change ./CHANGELOG.md.
    - Change ./requirements.txt and ./requirements-dev.txt.
    - Delete ./tddplatformpy directory and create your own source code directory.
    - Delete files under ./tests directory and create your own test code files.
    - Delete files under ./docs directory and create your own document files.
    - Delete ./console_sample directory.
    - Replace project name 'tddplatformpy' in ./.gitlab-ci.yml.

## Coverage report

Coverage report available at [GitLab Pages](https://2q3ridcz.gitlab.io/tdd-platform-py/)

## Reference

Directory structure in this project comes from these references:

- [あとで後悔しないPythonのディレクトリ構成をつくってみる - Qiita](https://qiita.com/kobori_akira/items/aa42790354654debb655)
- [4.7. プロジェクト構成 - ゼロから学ぶ Python](https://rinatz.github.io/python-book/ch04-07-project-structures/)
- [Structuring Your Project — The Hitchhiker's Guide to Python](https://docs.python-guide.org/writing/structure/)

GitLab CI references:

- [GitLab CIでflake8とpytestを実行 - Qiita](https://qiita.com/yuuwatanabe/items/a84bd050ba8342d9f0f0)
- [Test coverage visualization | GitLab](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html#python-example)
