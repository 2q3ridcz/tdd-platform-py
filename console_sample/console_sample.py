"""A sample script to use tddplatformpy package.

In console, set current directory to this file's directory,
and submit `python console_sample.py` to execute.
"""
# pylint: disable=wrong-import-position
import os
import sys

pkg_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if pkg_path not in sys.path:
    sys.path.insert(0, pkg_path)

from tddplatformpy import tddplatformpy


fb = tddplatformpy.FizzBuzz()
for i in range(17):
    print(i, fb.convert(num=i))
