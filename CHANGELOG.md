# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.3] - 2023-06-11
### Added
- Devcontainer
- requirements.txt and requirements-dev.txt

## [0.0.2] - 2022-06-25
### Added
- Pytest and flake8 to gitlab-ci.

## [0.0.1] - 2022-06-25
### Added
- Minimal set to start pytest.

[Unreleased]: https://gitlab.com/2q3ridcz/tdd-platform-py/-/compare/v0.0.3...main
[0.0.3]: https://gitlab.com/2q3ridcz/tdd-platform-py/-/tree/v0.0.3
[0.0.2]: https://gitlab.com/2q3ridcz/tdd-platform-py/-/tree/v0.0.2
[0.0.1]: https://gitlab.com/2q3ridcz/tdd-platform-py/-/tree/v0.0.1
