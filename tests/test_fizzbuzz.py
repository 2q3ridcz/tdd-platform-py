class TestConvertOtherNumToString:
    '''「数を文字列に変換する」が抽象度が高くテストしづらいので、具体的なテストとした。'''
    def test_give_1_return_1(self, fb):
        '''1を渡すと文字列 "1" を返す'''
        # テストコードは 検証 -> 実行 -> 準備の順に作成していく。
        # 準備
        # fb = fizzbuzz.FizzBuzz()
        # 実行
        actual = fb.convert(num=1)
        # 検証
        expected = "1"
        assert expected == actual

    def test_give_2_return_2(self, fb):
        '''2を渡すと文字列 "2" を返す'''
        # 準備
        # conftest.pyへ外出し
        # fb = fizzbuzz.FizzBuzz()
        # 実行
        actual = fb.convert(num=2)
        # 検証
        expected = "2"
        assert expected == actual


class TestConvertMultipleOf3ToFizz:
    '''「3の倍数のときは数の代わりに「Fizz」に変換する」'''
    def test_give_3_return_fizz(self, fb):
        '''3を渡すと文字列 "Fizz" を返す'''
        # 準備
        # conftest.pyへ外出し
        # fb = fizzbuzz.FizzBuzz()
        # 実行
        actual = fb.convert(num=3)
        # 検証
        expected = "Fizz"
        assert expected == actual


class TestConvertMultipleOf5ToBuzz:
    '''5の倍数のときは数の代わりに「Buzz」に変換する'''
    def test_give_5_return_buzz(self, fb):
        '''5を渡すと文字列 "Buzz" を返す'''
        # 準備
        # 実行
        actual = fb.convert(num=5)
        # 検証
        expected = "Buzz"
        assert expected == actual


class TestConvertMultipleOf3And5ToFizzBuzz:
    '''3と5両方の倍数のときは数の代わりに「FizzBuzz」に変換する'''
    def test_give_15_return_fizzbuzz(self, fb):
        '''15を渡すと文字列 "FizzBuzz" を返す'''
        # 準備
        # 実行
        actual = fb.convert(num=15)
        # 検証
        expected = "FizzBuzz"
        assert expected == actual
